import { Component } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';

class FormComponent extends Component {
    onFirstnameInputChangeHandler(event) {
        console.log(event.target.value);
    }

    onLastnameInputChangeHandler(event) {
        console.log(event.target.value);
    }

    onCountrySelectChangeHandler(event) {
        console.log(event.target.value);
    }

    onSubjectTextareaChangeHandler(event) {
        console.log(event.target.value);
    }

    onSubmit(event) {
        event.preventDefault();
        console.log("Form đã được submit");
    }

    render() {
        return <div style={{ backgroundColor: "#f8f7f7", padding: "1rem" }}>
            <form onSubmit={this.onSubmit}>
                <div className="form-group row mb-2">
                    <label className="col-sm-2 col-form-label">First Name</label>
                    <div className="col-sm-10">
                        <input type="text" className="form-control" placeholder="Your name" onChange={this.onFirstnameInputChangeHandler} />
                    </div>
                </div>

                <div className="form-group row mb-2">
                    <label className="col-sm-2 col-form-label">Last Name</label>
                    <div className="col-sm-10">
                        <input type="text" className="form-control" placeholder="Your last name" onChange={this.onLastnameInputChangeHandler} />
                    </div>
                </div>

                <div className="form-group row mb-2">
                    <label className="col-sm-2 col-form-label">Country</label>
                    <div className="col-sm-10">
                        <select className="form-control" onChange={this.onCountrySelectChangeHandler}>
                            <option>Australia</option>
                            <option>USA</option>
                            <option>VietNam</option>
                        </select>
                    </div>
                </div>

                <div className="form-group row mb-2">
                    <label className="col-sm-2 col-form-label">Subject</label>
                    <div className="col-sm-10">
                        <textarea className="form-control" placeholder="Write something..." onChange={this.onSubjectTextareaChangeHandler}></textarea>
                    </div>
                </div>

                <button className="btn btn-success">Send data</button>
            </form>
        </div>;
    }
}

export default FormComponent;