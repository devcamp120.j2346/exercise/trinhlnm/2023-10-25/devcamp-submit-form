import 'bootstrap/dist/css/bootstrap.min.css';
import Tittle from './components/tittle';
import FormComponent from './components/formComponent';

function App() {
  return (
    <div className='container'>
      <Tittle/>
      <FormComponent/>
    </div>
  );
}

export default App;
